using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Phone.Controls;
using GameStateManagement;
namespace GameStateManagementSample.Screens
{
    class creditsPage : GameScreen
    {
        Texture2D backgroundPic;
        InputAction backAction;
        SpriteFont text;
        Texture2D spLogo;
        Texture2D teamLogo;

        public creditsPage()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            backAction = new InputAction(
                new Buttons[] { Buttons.Start, Buttons.Back },
                new Keys[] { Keys.Escape },
                true);

        }
        public override void Activate(bool instancePreserved)
        {
            backgroundPic = ScreenManager.Game.Content.Load<Texture2D>("background");
            text = ScreenManager.Game.Content.Load<SpriteFont>("creditFont");

            spLogo = ScreenManager.Game.Content.Load<Texture2D>("splogo");
            teamLogo = ScreenManager.Game.Content.Load<Texture2D>("teamLogo");
            base.Activate(instancePreserved);
        }
        public override void Deactivate()
        {
            base.Deactivate();
        }
        public override void Unload()
        {
            base.Unload();
        }
        public override void HandleInput(GameTime gameTime, InputState input)
        {   // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            PlayerIndex playerindex;
            if (backAction.Evaluate(input, ControllingPlayer, out playerindex) || gamePadDisconnected)
            {
                LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen(),
                                                           new PhoneMainMenuScreen());
            }

            base.HandleInput(gameTime, input);
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            spriteBatch.Begin();
            spriteBatch.Draw(backgroundPic, new Rectangle(0, 0, viewport.Width, viewport.Height), new Color(TransitionAlpha, TransitionAlpha, TransitionAlpha));
            spriteBatch.DrawString(text, "Done by:", new Vector2(10, 420 - 360), Color.Black);
            spriteBatch.DrawString(text, "Team ditft2a05 :", new Vector2(10, 420 - 320), Color.Black);
            spriteBatch.DrawString(text, "+Zhou Zhihua", new Vector2(10, 420 - 280), Color.Black);
            spriteBatch.DrawString(text, " ->Main gameplay coder", new Vector2(10, 420 - 240), Color.Black);
            spriteBatch.DrawString(text, "+Tanrio Satria Junuan", new Vector2(10, 420 - 200), Color.Black);
            spriteBatch.DrawString(text, " ->Isolated Storage Coder, ", new Vector2(10, 420 - 160), Color.Black);
            spriteBatch.DrawString(text, " ->debugger, ", new Vector2(10, 420 - 120), Color.Black);
            spriteBatch.DrawString(text, "+Franklin Lim Bin Kai", new Vector2(10, 420 - 80), Color.Black);
            spriteBatch.DrawString(text, " ->Cross-Platform coder", new Vector2(10, 420 - 40), Color.Black);
            spriteBatch.DrawString(text, " ->Artwork provider, debugger", new Vector2(10, 420), Color.Black);
            spriteBatch.DrawString(text, "School of Digital Media &", new Vector2(10, 620 - 120), Color.Black);
            spriteBatch.DrawString(text, "Infocomm technology", new Vector2(10, 620 - 80), Color.Black);
            spriteBatch.DrawString(text, "Singapore Polytechnic", new Vector2(10, 620 - 40), Color.Black);
            spriteBatch.DrawString(text, "Credits to Microsoft.", new Vector2(10, 620), Color.Black);
            spriteBatch.Draw(spLogo, new Vector2(10, 620 + 40), Color.White);
            spriteBatch.Draw(teamLogo, new Rectangle(280, 70, 100, 100), Color.White);

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
