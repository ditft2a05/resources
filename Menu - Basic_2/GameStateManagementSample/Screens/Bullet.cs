using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using GameStateManagement;
using System.Collections.Generic;
using System.Text;

namespace GameStateManagementSample
{
    class Bullet
    {
        #region Fields

        Vector2 position;

        Rectangle bulletSourceRect;
        Rectangle bulletDestinationRect;

        bool life;
        double yDelta;
        double angle;
        bool spawnAbove;

        int frame;

        #endregion

        #region constructor
        public Bullet()//default
        {
            position = new Vector2(-100, -100);
            life = false;
            yDelta = 0;
            angle = 0;
            frame = 0;
            bulletSourceRect = new Rectangle(0, 0, 48, 60);
            bulletDestinationRect = new Rectangle(0, 0, 48, 60);
            
        }

        public Bullet(Vector2 pos, bool l, double yD, double an)//constructor with parameters
        {
            position = pos;
            life = l;
            yDelta = yD;
            angle = an;
            frame = 0;
            bulletSourceRect = new Rectangle(0, 0, 48, 60);
            bulletDestinationRect = new Rectangle((int)pos.X, (int)pos.Y, 48, 60);
            
        }
        #endregion

        public void fly()
        {
            bulletSourceRect.X = frame * 48;
            if (spawnAbove)
            {

                bulletDestinationRect.X += (int)(yDelta * Math.Tan(angle) / 20);
                bulletDestinationRect.Y -= (int)(yDelta / 20);
            }
            else
            {
                bulletDestinationRect.X -= (int)(yDelta * Math.Tan(angle) / 20);
                bulletDestinationRect.Y -= (int)(yDelta / 20);
            }

            if ((yDelta * Math.Tan(angle) / 20) < 3)
            {
                if ((yDelta * Math.Tan(angle) / 20) > -3)
                {
                    if ((yDelta / 20) < 3)
                    {
                        if (yDelta / 20 > -3)
                        {
                            life = false;
                        }
                    }
                }
            }
        }

        public bool isAlive()
        {
            return life;
        }

        public void reset()
        {
            bulletDestinationRect.X = -100;
            bulletDestinationRect.Y = -100;
            life = false;
            yDelta = 0;
            angle = 0;
        }

        public Vector2 getPosition()
        {
            return position;
        }

        public void setPosition(Vector2 pos)
        {
            position = pos;
        }

        public void setLife(bool l)
        {
            life = l;
        }

        public void setyDelta(double yD)
        {
            yDelta = yD;
        }

        public void setAngle(double a)
        {
            angle = a;
        }

        public void setSpawnAbove(bool sa)
        {
            spawnAbove = sa;
        }

        public Rectangle getSourceRect()
        {
            return bulletSourceRect;
        }

        public Rectangle getDestinationRect()
        {
            return bulletDestinationRect;
        }

        public void setDestinationRectXY(int x, int y)
        {
            bulletDestinationRect.X = x;
            bulletDestinationRect.Y = y;
        }

        public void setFrame(int f)
        {
            frame = f;
        }
    }
}
