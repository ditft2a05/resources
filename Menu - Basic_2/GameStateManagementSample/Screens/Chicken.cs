using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using GameStateManagement;
using System.Collections.Generic;
using System.Text;

namespace GameStateManagementSample
{
    class Chicken
    {
        #region field

        bool life;
        Rectangle chickenSourceRect;
        Rectangle chickenDestinationRect;
        int type;//stand for type of chicken sprite use, calculated from spawnPos
        Vector2 spawnPos;//position of the chicken spawn
        int frame;
        int speed;

        #endregion


        #region constructor

        public Chicken()
        {
            speed = 2;
            life = false;
            chickenSourceRect = new Rectangle(0, 0, 32, 32);
            chickenDestinationRect = new Rectangle(900, 900, 32, 32);
            type = 0;
            spawnPos = new Vector2(0, 0);
            frame = 0;
        }

        #endregion


        #region function

        public void setSpeed(int i)
        {
            speed = i;
        }

        public void attack()
        {
            chickenSourceRect.X = frame * 32;

            if (type == 0)
            {
                if (spawnPos.X <= 180)
                {
                    chickenDestinationRect.Y += speed;
                    chickenDestinationRect.X += speed;
                }
                else if (spawnPos.X >= 300)
                {
                    chickenDestinationRect.Y += speed;
                    chickenDestinationRect.X -= speed;
                }
                else
                {
                    chickenDestinationRect.Y += speed;
                }
            }
            else if (type == 2)
            {
                chickenDestinationRect.X += speed;
            }
            else if (type == 1)
            {
                chickenDestinationRect.X -= speed;
            }
            else if (type == 3)
            {
                if (spawnPos.X < 180)
                {
                    chickenDestinationRect.Y -= speed;
                    chickenDestinationRect.X += speed;
                }
                else if (spawnPos.X > 300)
                {
                    chickenDestinationRect.Y -= speed;
                    chickenDestinationRect.X -= speed;
                }
                else
                {
                    chickenDestinationRect.Y -= speed;
                }
            }
        }

        public void setFrame(int f)
        {
            frame = f;
        }

        public void setPosition(Vector2 v)
        {
            spawnPos.X = v.X;
            spawnPos.Y = v.Y;
            chickenDestinationRect.X = (int)v.X;
            chickenDestinationRect.Y = (int)v.Y;

            //calculate type of chicken sprite use
            if (spawnPos.Y <= 200)
            {
                type = 0;
            }
            else if (spawnPos.Y <= 600)
            {
                if (spawnPos.X <= 280)
                {
                    type = 2;
                }
                else
                {
                    type = 1;
                }
            }
            else if (spawnPos.Y <= 800)
            {
                type = 3;
            }
            else
            {
                type = 0;
            }

            //set chicken to that type of sprite
            chickenSourceRect.Y = type * 32;
        }

        public bool isAlive()
        {
            return life;
        }

        public void reset()
        {
            chickenDestinationRect.X = -200;
            chickenDestinationRect.Y = -200;
            life = false;
            spawnPos.X = 0;
            spawnPos.Y = 0;
            type = 0;
            frame = 0;
        }

        public Rectangle getDestinationRect()
        {
            return chickenDestinationRect;
        }

        public Rectangle getSourceRect()
        {
            return chickenSourceRect;
        }

        public void setLife(bool l)
        {
            life = l;
        }

        #endregion
    }
}
