#region File Description
//-----------------------------------------------------------------------------
// PhoneMainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

using System;
using GameStateManagement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Media;
using System.IO.IsolatedStorage;
namespace GameStateManagementSample
{
    class PhoneMainMenuScreen : PhoneMenuScreen
    {   
        BackgroundMusicManager musicManager;
        Song bg;
        IsolatedStorageSettings savegameStorage;
        SoundEffect click01;
        public static bool sfxOn = true;
        public static bool bgmOn = true;
        bool checkMusic;
        BooleanButton sfxButton;
        BooleanButton musicButton;
        public PhoneMainMenuScreen()
            : base("Chicken")
        {
            LoadSoundState();
            // Create a button to start the game
            Button playButton = new Button("Play");
            playButton.Tapped += playButton_Tapped;
            MenuButtons.Add(playButton);
            
            // Create two buttons to toggle sound effects and music. This sample just shows one way
            // of making and using these buttons; it doesn't actually have sound effects or music
            sfxButton = new BooleanButton("Sound Effects", sfxOn);
            sfxButton.Tapped += sfxButton_Tapped;
            
            MenuButtons.Add(sfxButton);

            if (bgmOn)
            {
                checkMusic = true;
            }
            else if (bgmOn == false)
            {
                checkMusic = false;
            }

            musicButton = new BooleanButton("Music", checkMusic);
            musicButton.Tapped += musicButton_Tapped;
            MenuButtons.Add(musicButton);

            Button reset = new Button("Reset Sound");
            reset.Tapped += reset_Tapped;
            MenuButtons.Add(reset);

            Button resetScore = new Button("Reset Score");
            resetScore.Tapped += resetScore_Tapped;
            MenuButtons.Add(resetScore);

            Button helpButton = new Button("Help");
            helpButton.Tapped += helpButton_Tapped;
            MenuButtons.Add(helpButton);

            Button creditsButton = new Button("Credits");
            creditsButton.Tapped += creditsButton_Tapped;
            MenuButtons.Add(creditsButton);

            
        }



        public override void Activate(bool instancePreserved)
        {
            click01 = ScreenManager.Game.Content.Load<SoundEffect>("UI_Misc05");
            bg = this.ScreenManager.Game.Content.Load<Song>("Azaleas");
            musicManager = (BackgroundMusicManager)this.ScreenManager.Game.Services.GetService(typeof(BackgroundMusicManager)) as BackgroundMusicManager;
            base.Activate(instancePreserved);
            if (bgmOn == true)
            {
                musicManager.Play(bg);
            }
            else if (bgmOn == false)
            {
                musicManager.Stop();
            }
        }

        void playButton_Tapped(object sender, EventArgs e)
        {
            SaveSoundState();
            if (sfxOn == true)
            {
                click01.Play();
            }
            // When the "Play" button is tapped, we load the GameplayScreen
            LoadingScreen.Load(ScreenManager, true, PlayerIndex.One, new GameplayScreen());
        }

        void sfxButton_Tapped(object sender, EventArgs e)
        {
            BooleanButton button = sender as BooleanButton;
            if (sfxOn == true)
            {
                click01.Play();
                sfxOn = false;
            }
            else if (sfxOn == false)
            {
                sfxOn = true;

            }
            // In a real game, you'd want to store away the value of 
            // the button to turn off sounds here. :)
        }

        void musicButton_Tapped(object sender, EventArgs e)
        {
            BooleanButton button = sender as BooleanButton;

            if (sfxOn == true)
            {
                click01.Play();
            }


            if (bgmOn == true)
            {
                bgmOn = false;
            }
            else if (bgmOn == false)
            {
                bgmOn = true;
            }
            toogleMusic(bgmOn);
            // In a real game, you'd want to store away the value of 
            // the button to turn off music here. :)
        }

        void toogleMusic(bool b)
        {

            if (b == true)
            {
                musicManager.Play(bg);
            }
            else
            {
                musicManager.Stop();
            }

        }

        void reset_Tapped(object sender, EventArgs e)
        {
            if (sfxOn == true)
            {
                click01.Play();
            }
            ResetSoundState();
            SaveSoundState();
        }

        void resetScore_Tapped(object sender, EventArgs e)
        {
            if (sfxOn == true)
            {
                click01.Play();
            }

            resetScore();
        }

        void helpButton_Tapped(object sender, EventArgs e)
        {
            SaveSoundState();
            if (sfxOn == true)
            {
                click01.Play();
            }
            LoadingScreen.Load(ScreenManager, true, PlayerIndex.One, new GameStateManagementSample.Screens.Help());
            
        }

        void creditsButton_Tapped(object sender, EventArgs e)
        {
            SaveSoundState();
            if (sfxOn == true)
            {
                click01.Play();
            }
            LoadingScreen.Load(ScreenManager, true, PlayerIndex.One, new GameStateManagementSample.Screens.creditsPage());

        }

        protected override void OnCancel()
        {
            SaveSoundState();
            ScreenManager.Game.Exit();
            base.OnCancel();
        }


        //Method to call save for the sfx and bgm with IsolatedStorageSettings.
        protected void SaveSoundState()
        {
            if (savegameStorage.Contains("SoundState"))
            {
                savegameStorage["SoundState"] = sfxOn;
            }
            else
            {
                savegameStorage.Add("SoundState", sfxOn);
            }

            if (savegameStorage.Contains("MusicState"))
            {
                savegameStorage["MusicState"] = bgmOn;
            }
            else
            {
                savegameStorage.Add("MusicState", bgmOn);
            }
            savegameStorage.Save();
        }

        //Method to call load sfx and bgm state with IsolatedStorageSettings.
        protected void LoadSoundState()
        {
            savegameStorage = IsolatedStorageSettings.ApplicationSettings;
            if (savegameStorage.Contains("SoundState"))
            {
                sfxOn= (bool)savegameStorage["SoundState"];
            }

            if (savegameStorage.Contains("MusicState"))
            {
                bgmOn = (bool)savegameStorage["MusicState"];
            }

        }
        //Method to call reset to the sfx and bgm state to true.
        protected void ResetSoundState()
        {
            if (sfxOn != true)
            {
                sfxButton.OnTapped();
            }
            if (bgmOn != true)
            {
                musicButton.OnTapped();
            }
            
        }
        //Method to reset highscore
        protected void resetScore()
        {
            IsolatedStorageFile saveStorage = IsolatedStorageFile.GetUserStoreForApplication();
            // open isolated storage, and write the savefile.
            IsolatedStorageFileStream fs = null;
            using (fs = saveStorage.CreateFile(GameplayScreen.filename))
            {
                if (fs != null)
                {
                    // just overwrite the existing info for this example.
                    byte[] bytes = System.BitConverter.GetBytes(0);
                    fs.Write(bytes, 0, bytes.Length);
                    GameplayScreen.highScore = 0;
                }
            }
        }
    }
}
