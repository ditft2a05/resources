#region File Description
//-----------------------------------------------------------------------------
// GameplayScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using GameStateManagement;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
#endregion

namespace GameStateManagementSample
{
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>
    class GameplayScreen : GameScreen
    {
        #region Fields

        double i = 0;
        double j = 0;
        int score = 0;
        public static int highScore = 0;
        int life = 20;
        Song song;
        public static string filename = "lel";
        ContentManager content;
        Random random = new Random();
        float pauseAlpha;
        InputAction pauseAction;

        SoundEffect chicken;

        SpriteFont font;
        Texture2D white;
        string info = "";
        Texture2D ball;
        Rectangle ballSpawn = new Rectangle(0, 0, 64, 64);
        double angle = 0;
        int yDelta = 0;
        bool spawnAbove = false;
        int elapsedTime = 0;
        int frameTime = 300;
        int frame = 0;
        int chickenFrame = 0;

        Rectangle storeRect = new Rectangle(180, 200, 120, 400);
        
        Texture2D bulletTexture;
        Bullet[] bulletList = new Bullet[20];
        Vector2 bulletPosition;

        Texture2D chickenTexture;
        Chicken[] chickenList = new Chicken[20];
        Vector2 chickenPos = new Vector2(0, 0);

        #endregion

        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        public GameplayScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            score = 0;
            pauseAction = new InputAction(
                new Buttons[] { Buttons.Start, Buttons.Back },
                new Keys[] { Keys.Escape },
                true);

            this.EnabledGestures = GestureType.Tap | GestureType.FreeDrag;

            Load();
        }


        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");
                white = this.ScreenManager.Game.Content.Load<Texture2D>("corn-storage");
                font = this.ScreenManager.Game.Content.Load<SpriteFont>("font");
                ball = this.ScreenManager.Game.Content.Load<Texture2D>("ball");
                bulletTexture = this.ScreenManager.Game.Content.Load<Texture2D>("fireball");
                chickenTexture = this.ScreenManager.Game.Content.Load<Texture2D>("chickenSpriteSheet");
                chicken = this.ScreenManager.Game.Content.Load<SoundEffect>("UI_Misc06");
                song = ScreenManager.Game.Content.Load<Song>("Azaleas");
                for (int i = 0; i < 20; i++)
                {
                    bulletList[i] = new Bullet();
                    chickenList[i] = new Chicken();
                }


                song = ScreenManager.Game.Content.Load<Song>("Azaleas");
                BackgroundMusicManager mgr = (BackgroundMusicManager)
                this.ScreenManager.Game.Services.GetService(typeof(BackgroundMusicManager));
                if (PhoneMainMenuScreen.bgmOn == true)
                {
                    mgr.Play(song);
                }
                else {
                    mgr.Stop();
                }

                // A real game would probably have more content than this sample, so
                // it would take longer to load. We simulate that by delaying for a
                // while, giving you a chance to admire the beautiful loading screen.
                Thread.Sleep(1000);

                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.
                ScreenManager.Game.ResetElapsedTime();
            }
        }


        public override void Deactivate()
        {
            base.Deactivate();
        }


        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            content.Unload();
        }
        #endregion

        #region Update and Draw

        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            score += (int)gameTime.ElapsedGameTime.TotalMilliseconds;
            info = (score / 1000).ToString();

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                //// TODO: Add your update logic here
                
                elapsedTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (elapsedTime > frameTime)
                {
                    //frame for bullet
                    frame++;

                    //frame for chicken
                    if (chickenFrame == 0)
                        chickenFrame++;
                    if (chickenFrame == 2)
                        chickenFrame--;
                    
                    elapsedTime = 0;

                    if (frame > 3)
                    {
                        //reset frame for bullet
                        frame = 0;
                    }
                }

                //bullet logics
                //if bullet is alive, call fly function, change the frame, if bullet flies out of bounds, reset the bullet.
                foreach (Bullet b in bulletList)
                {
                    if (b.isAlive())
                    {
                        b.fly();
                        b.setFrame(frame);

                        if (b.getDestinationRect().X >= 480)
                        {
                            b.reset();
                        }
                        else if (b.getDestinationRect().X <= -64)
                        {
                            b.reset();
                        }
                        else if (b.getDestinationRect().Y >= 800)
                        {
                            b.reset();
                        }
                        else if (b.getDestinationRect().Y <= -64)
                        {
                            b.reset();
                        }
                    }
                }

                //logics for chicken
                //if i is less than 0.01, spawn chicken randomly
                //chicken run out of bound kill, chichen run into target position kill
                //if bullet hit chicken, kill chicken and bullet
                foreach (Chicken c in chickenList)
                {
                    i = random.NextDouble();
                    j = random.NextDouble();
                    if  (i < 0.03)
                    {
                        if (!c.isAlive())
                        {
                            if (j < 0.25)
                            {
                                chickenPos.X = 50;
                                chickenPos.Y = (int)(i * 33 * 750);
                            }
                            else if (j < 0.5)
                            {
                                chickenPos.X = (int)(i * 33 * 430);
                                chickenPos.Y = 50;
                            }
                            else if (j < 0.75)
                            {
                                chickenPos.X = 430;
                                chickenPos.Y = (int)(i * 33 * 750);
                            }
                            else
                            {
                                chickenPos.X = (int)(i * 33 * 430);
                                chickenPos.Y = 750;
                            }
                            
                            c.setPosition(chickenPos);
                            c.setLife(true);
                            
                            break;
                        }
                    }
                    
                    if (c.isAlive())
                    {
                        c.attack();
                        c.setFrame(chickenFrame);

                        if (c.getDestinationRect().Intersects(storeRect))
                        {
                            if (PhoneMainMenuScreen.sfxOn == true)
                            {
                                chicken.Play();
                            }
                            c.reset();
                            life--;
                        }

                        if (c.getDestinationRect().X >= 500)
                        {
                            c.reset();
                        }
                        else if (c.getDestinationRect().X <= -20)
                        {
                            c.reset();
                        }
                        else if (c.getDestinationRect().Y >= 820)
                        {
                            c.reset();
                        }
                        else if (c.getDestinationRect().Y <= -20)
                        {
                            c.reset();
                        }
                    }//if chicken alive loop

                    foreach (Bullet b in bulletList)
                    {
                        if (b.getDestinationRect().Intersects(c.getDestinationRect()))
                        {
                            b.reset();
                            c.reset();
                        }
                        
                    }
                }//foreach chicken loop

                if (life < 0)
                {
                    if (score > highScore)
                    {
                        Save();
                    }
                    ScreenManager.AddScreen(new DieScreen((int)(score/1000)), ControllingPlayer);
                }
                

            }//if active loop
        }


        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(GameTime gameTime, InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            PlayerIndex player;
            if (pauseAction.Evaluate(input, ControllingPlayer, out player) || gamePadDisconnected)
            {
#if WINDOWS_PHONE
                ScreenManager.AddScreen(new PhonePauseScreen(), ControllingPlayer);
#else
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
#endif
            }
            else
            {
                foreach (GestureSample sample in input.Gestures)
                {
                    if (sample.GestureType == GestureType.Tap)
                    {
                        //ballFSM = 1;
                        ballSpawn.X = (int)sample.Position.X - 32;
                        ballSpawn.Y = (int)sample.Position.Y - 32;
                    }

                    if (sample.GestureType == GestureType.FreeDrag)
                    {
                        bulletPosition.X = (int)sample.Position.X;
                        bulletPosition.Y = (int)sample.Position.Y;

                        yDelta = (int)(sample.Position.Y - ballSpawn.Y - 32);

                        if (sample.Position.Y >= ballSpawn.Y)
                        {
                            spawnAbove = false;
                            angle = Math.Atan((sample.Position.X - ballSpawn.X - 32) / (sample.Position.Y - ballSpawn.Y - 32));
                        }
                        else
                        {
                            spawnAbove = true;
                            angle = (-1) * (Math.Atan((sample.Position.X - ballSpawn.X - 32) / (sample.Position.Y - ballSpawn.Y - 32)));
                        }
                        foreach (Bullet b in bulletList)
                        {
                            
                            if (!(b.isAlive()))
                            {
                                b.setDestinationRectXY((int)bulletPosition.X, (int)bulletPosition.Y);
                                b.setyDelta(yDelta);
                                b.setAngle(angle);
                                b.setSpawnAbove(spawnAbove);
                                b.setLife(true);
                                break;
                            }
                        }

                    }
                }
            }
        }


        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // This game has a blue background. Why? Because!
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.CornflowerBlue, 0, 0);

            // Our player and enemy are both actually just text strings.
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin();
            spriteBatch.Draw(white, new Rectangle(0, 0, 480, 800), Color.White);
            spriteBatch.DrawString(font, "Time survived :" + info, new Vector2(50, 70), Color.Black);
            spriteBatch.DrawString(font, "Life remaining : " + life.ToString(), new Vector2(50, 120), Color.Black);
            //if (ballFSM == 1)
            //{
            //    spriteBatch.Draw(ball, ballSpawn, Color.White);
            //}
            foreach (Bullet b in bulletList)
            {
                if (b.isAlive())
                {
                    spriteBatch.Draw(bulletTexture, b.getDestinationRect(), b.getSourceRect(), Color.White);
                } 
            }

            foreach (Chicken c in chickenList)
            {
                if (c.isAlive())
                {
                    spriteBatch.Draw(chickenTexture, c.getDestinationRect(), c.getSourceRect(), Color.White);
                }
            }
            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }


        #endregion

        #region Save/Load
        //Method to save with IsolatedStorageFile.
        void Save()
        {
            IsolatedStorageFile saveStorage = IsolatedStorageFile.GetUserStoreForApplication();
            // open isolated storage, and write the savefile.
            IsolatedStorageFileStream fs = null;
            using (fs = saveStorage.CreateFile(filename))
            {
                if (fs != null)
                {
                    // just overwrite the existing info for this example.
                    byte[] bytes = System.BitConverter.GetBytes(score);
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
        }
        //Method to load with IsolatedStorageFile.
        void Load()
        {
            using (IsolatedStorageFile saveStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (saveStorage.FileExists(filename))
                {
                    using (IsolatedStorageFileStream fs = saveStorage.OpenFile(filename, System.IO.FileMode.Open))
                    {
                        if (fs != null)
                        {
                            // Reload the saved high-score data.
                            byte[] saveBytes = new byte[fs.Length];
                            int count = fs.Read(saveBytes, 0, saveBytes.Length);
                            if (count > 0)
                            {
                                highScore = System.BitConverter.ToInt32(saveBytes,0);
                            }
                        }
                    }
                }
            }
        }
        
        #endregion

    }
}
