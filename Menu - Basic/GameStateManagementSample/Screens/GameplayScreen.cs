#region File Description
//-----------------------------------------------------------------------------
// GameplayScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Devices;
using GameStateManagement;
using Microsoft.Xna.Framework.Input.Touch;
#endregion

namespace GameStateManagementSample
{
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>
    class GameplayScreen : GameScreen
    {
        #region Fields

        ContentManager content;
        SpriteFont gameFont;

        Random random = new Random();

        float pauseAlpha;

        InputAction pauseAction;

        Texture2D backgroundTexture;

        Vector2 wordsPosition = new Vector2(0, 20);
        Vector2 wordsPosition2 = new Vector2(0, 50);

        Rectangle blueCircle = new Rectangle(20, 150, 220, 220);
        Rectangle yellowCircle = new Rectangle(240, 150, 220, 220);
        Rectangle redCircle = new Rectangle(20, 370, 220, 220);
        Rectangle greenCircle = new Rectangle(240, 370, 220, 220);
        Rectangle orangeCircle = new Rectangle(20, 590, 220, 220);
        Rectangle purpleCircle = new Rectangle(240, 590, 220, 220);
        
        Rectangle currCircle = new Rectangle(0, 0, 220, 220);
        int currCircleNo = 0;

        int playerTurn;
        int[] pCircle = { 0, 0, 0, 0 };

        string text = "";
        string text2 = "";

        Texture2D[] circle = new Texture2D[6];

        bool blue = false;
        bool yellow = false;
        bool red = false;
        bool green = false;
        bool orange = false;
        bool purple = false;

        bool gameover = false;

        double elapsedTime = 0.0;

        bool randBool = true;
        int randInt = 0;
        #endregion

        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        Song song;
        

        public GameplayScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            pauseAction = new InputAction(
                new Buttons[] { Buttons.Start, Buttons.Back },
                new Keys[] { Keys.Escape },
                true);
        }
        public void reset() 
        {
            blue = false;
            yellow = false;
            red = false;
            green = false;
            orange = false;
            purple = false;
            playerTurn = 0;
            currCircleNo = 0;
            pCircle[0] =0;
            pCircle[1] = 0;
            pCircle[2] = 0;
            pCircle[3] = 0;
            gameover = false;
            text = "";
            elapsedTime = 0.0;
            int i;
                for (int j = 0; j < pCircle.Length; j++)
                {
                    do
                    {
                        i = (int)(random.NextDouble() * 7);
                    }
                    while (i == pCircle[0] || i == pCircle[1] || i == pCircle[2] || i == pCircle[3]);
                    pCircle[j] = i;

                }
            gameplay();
        
        }

        public void gameplay()
        {
            if (playerTurn < 4)
            {
                playerTurn++;

                switch (pCircle[playerTurn - 1])
                {
                    case (1): currCircle = blueCircle;
                        currCircleNo = 0;
                        text = "Player " + playerTurn + ": Blue Circle";
                        break;
                    case (2): currCircle = yellowCircle;
                        currCircleNo = 1;
                        text = "Player " + playerTurn + ": Yellow Circle";
                        break;
                    case (3): currCircle = redCircle;
                        currCircleNo = 2;
                        text = "Player " + playerTurn + ": Red Circle";
                        break;
                    case (4): currCircle = greenCircle;
                        currCircleNo = 3;
                        text = "Player " + playerTurn + ": Green Circle";
                        break;
                    case (5): currCircle = orangeCircle;
                        currCircleNo = 4;
                        text = "Player " + playerTurn + ": Orange Circle";
                        break;
                    case (6): currCircle = purpleCircle;
                        currCircleNo = 5;
                        text = "Player " + playerTurn + ": Purple Circle";
                        break;
                }

            }
        }
        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                gameFont = content.Load<SpriteFont>("menufont");
                backgroundTexture = content.Load<Texture2D>("backgroundFinal");
                song = ScreenManager.Game.Content.Load<Song>("Azaleas");

                int i;
                for (int j = 0; j < pCircle.Length; j++)
                {
                    do
                    {
                        i = (int)(random.NextDouble() * 7);
                    }
                    while (i == pCircle[0] || i == pCircle[1] || i == pCircle[2] || i == pCircle[3]);
                    pCircle[j] = i;

                }
                circle[0] = content.Load<Texture2D>("blueCCi");
                circle[1] = content.Load<Texture2D>("yellowCCi");
                circle[2] = content.Load<Texture2D>("redCCi");
                circle[3] = content.Load<Texture2D>("greenCCi");
                circle[4] = content.Load<Texture2D>("orangeCCi");
                circle[5] = content.Load<Texture2D>("purpleCCi");
                gameplay();

                BackgroundMusicManager mgr = (BackgroundMusicManager)
                this.ScreenManager.Game.Services.GetService(typeof(BackgroundMusicManager));
                if (PhoneMainMenuScreen.bgmOn)
                {
                    mgr.Play(song);
                }
                else if (PhoneMainMenuScreen.bgmOn == false)
                {
                    mgr.Stop();
                }
                // A real game would probably have more content than this sample, so
                // it would take longer to load. We simulate that by delaying for a
                // while, giving you a chance to admire the beautiful loading screen.
                Thread.Sleep(1000);

                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.
                ScreenManager.Game.ResetElapsedTime();
            }

        }


        public override void Deactivate()
        {

            base.Deactivate();
        }


        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            content.Unload();

        }


        #endregion

        #region Update and Draw


        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                elapsedTime += gameTime.ElapsedGameTime.TotalSeconds;
                if (randBool)
                {
                    randInt = (int)(random.NextDouble() * 2);
                    randBool = false;
                }
                if (elapsedTime > 15 && !gameover)
                {
                    if (randInt == 0)
                    {
                        text2 = "Tilt the phone \n45 degrees to your left.";
                        Vector3 acceleration = Accelerometer.GetState().Acceleration;

                        if ((acceleration.X < -0.5) && (acceleration.Z < -0.5))
                        {
                            text2 = "Success";
                            elapsedTime = 0;
                            randBool=true;
                        }
                    }
                    else
                    {
                        text2 = "Tilt the phone \n45 degrees to your right.";
                        Vector3 acceleration = Accelerometer.GetState().Acceleration;

                        if ((acceleration.X > 0.5) && (acceleration.Z > -0.5))
                        {
                            text2 = "Success";
                            elapsedTime = 0;
                            randBool = true;
                        }
                    }
                }
                

            }
        }


        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(GameTime gameTime, InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            PlayerIndex player;
            if (pauseAction.Evaluate(input, ControllingPlayer, out player) || gamePadDisconnected)
            {
#if WINDOWS_PHONE
                ScreenManager.AddScreen(new PhonePauseScreen(), ControllingPlayer);
#else
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
#endif
            }
            else
            {

                TouchCollection touchCollection = TouchPanel.GetState();

                if (gameover)
                {
                    if (ScreenManager.vibrator != null)
                    {
                        ScreenManager.vibrator.Start(TimeSpan.FromSeconds(0.1f));
                    }
                    text2 = "";
                    foreach (TouchLocation tl in touchCollection)
                        if (tl.State == TouchLocationState.Pressed)
                            reset();
                }else foreach (TouchLocation tl in touchCollection)
                {
                    Point holdLocation;
                    if ((tl.State == TouchLocationState.Pressed)
                            || (tl.State == TouchLocationState.Moved))
                    {
                        holdLocation = new Point((int)touchCollection[0].Position.X, (int)touchCollection[0].Position.Y);
                        if (currCircle.Contains(holdLocation))
                        {
                            switch (currCircleNo)
                            {
                                case (0): blue = true;
                                    break;
                                case (1): yellow = true;
                                    break;
                                case (2): red = true;
                                    break;
                                case (3): green = true;
                                    break;
                                case (4): orange = true;
                                    break;
                                case (5): purple = true;
                                    break;
                            }
                            gameplay();
                        }
                    }
                    if (tl.State == TouchLocationState.Moved)
                    {
                        for (int i = 0; i < touchCollection.Count; i++)
                        {
                            holdLocation = new Point((int)touchCollection[i].Position.X, (int)touchCollection[i].Position.Y);

                            if (!blueCircle.Contains(holdLocation))
                                blue = false;
                            if (!yellowCircle.Contains(holdLocation))
                                yellow = false;
                            if (!redCircle.Contains(holdLocation))
                                red = false;
                            if (!greenCircle.Contains(holdLocation))
                                green = false;
                            if (!orangeCircle.Contains(holdLocation))
                                orange = false;
                            if (!purpleCircle.Contains(holdLocation))
                                purple = false;

                            for (int k = 0; k < playerTurn - 1; k++)
                            {
                                switch (pCircle[k])
                                {
                                    case (1):
                                        if (!blueCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                            break;
                                        
                                    case (2):
                                        if (!yellowCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                        break;
                                    case (3):
                                        if (!redCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                        break;
                                    case (4):
                                        if (!greenCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                        break;
                                    case (5):
                                        if (!orangeCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                        break;
                                    case (6):
                                        if (!purpleCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                        break;
                                }

                            }
                        }
                    }
                    if (tl.State == TouchLocationState.Released)
                    {
                        for (int i = 0; i < touchCollection.Count; i++)
                        {
                            holdLocation = new Point((int)touchCollection[i].Position.X, (int)touchCollection[i].Position.Y);

                            if (blueCircle.Contains(holdLocation))
                                blue = false;
                            if (yellowCircle.Contains(holdLocation))
                                yellow = false;
                            if (redCircle.Contains(holdLocation))
                                red = false;
                            if (greenCircle.Contains(holdLocation))
                                green = false;
                            if (orangeCircle.Contains(holdLocation))
                                orange = false;
                            if (purpleCircle.Contains(holdLocation))
                                purple = false;
                            for (int k = 0; k < playerTurn - 1; k++)
                            {
                                switch (pCircle[k])
                                {
                                    case (1):
                                        if (blueCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                        break;
                                    case (2):
                                        if (yellowCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                        break;
                                    case (3):
                                        if (redCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                        break;
                                    case (4):
                                        if (greenCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                        break;
                                    case (5):
                                        if (orangeCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                        break;
                                    case (6):
                                        if (purpleCircle.Contains(holdLocation))
                                        {
                                            text = "Player " + (k + 1) + " has lost. \nClick anywhere to retry";
                                            gameover = true;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }



        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // This game has a blue background. Why? Because!
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.CornflowerBlue, 0, 0);

            // Our player and enemy are both actually just text strings.
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);

            spriteBatch.Begin();
            spriteBatch.Draw(backgroundTexture, fullscreen,
                              new Color(TransitionAlpha, TransitionAlpha, TransitionAlpha));
            spriteBatch.DrawString(gameFont, text, wordsPosition, Color.White);
            spriteBatch.DrawString(gameFont, text2, wordsPosition2, Color.White);
            if (blue)
                spriteBatch.Draw(circle[0], blueCircle, Color.White);
            if (yellow)
                spriteBatch.Draw(circle[1], yellowCircle, Color.White);
            if (red)
                spriteBatch.Draw(circle[2], redCircle, Color.White);
            if (green)
                spriteBatch.Draw(circle[3], greenCircle, Color.White);
            if (orange)
                spriteBatch.Draw(circle[4], orangeCircle, Color.White);
            if (purple)
                spriteBatch.Draw(circle[5], purpleCircle, Color.White);
            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }


        #endregion
    }
}
