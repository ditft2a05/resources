#region File Description
//-----------------------------------------------------------------------------
// PhoneMainMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

using System;
using GameStateManagement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Media;

namespace GameStateManagementSample
{
    class PhoneMainMenuScreen : PhoneMenuScreen
    {
        BackgroundMusicManager musicManager;
        Song bg;

        SoundEffect click01;
        public static bool sfxOn = true;
        public static bool bgmOn = true;
        bool checkMusic;
        public PhoneMainMenuScreen()
            : base("Phone Twister")
        {
            //Create a button to start the game
            Button playButton = new Button("Play");
            playButton.Tapped += playButton_Tapped;
            MenuButtons.Add(playButton);

            // Create two buttons to toggle sound effects and music. This sample just shows one way
            // of making and using these buttons; it doesn't actually have sound effects or music
            BooleanButton sfxButton = new BooleanButton("Sound Effects", true);
            sfxButton.Tapped += sfxButton_Tapped;
            MenuButtons.Add(sfxButton);

            if (bgmOn)
            {
                checkMusic = true;
            }
            else if (bgmOn == false)
            {
                checkMusic = false;
            }

            BooleanButton musicButton = new BooleanButton("Music", checkMusic);
            musicButton.Tapped += musicButton_Tapped;
            MenuButtons.Add(musicButton);

            Button helpButton = new Button("Help");
            helpButton.Tapped += helpButton_Tapped;
            MenuButtons.Add(helpButton);

            Button creditsButton = new Button("Credits");
            creditsButton.Tapped += creditsButton_Tapped;
            MenuButtons.Add(creditsButton);

        }
        
        public override void Activate(bool instancePreserved)
        {
            click01 = ScreenManager.Game.Content.Load<SoundEffect>("UI_Misc05");
            bg = this.ScreenManager.Game.Content.Load<Song>("Azaleas");
            musicManager = (BackgroundMusicManager)this.ScreenManager.Game.Services.GetService(typeof(BackgroundMusicManager)) as BackgroundMusicManager;
            base.Activate(instancePreserved);

        }
        void playButton_Tapped(object sender, EventArgs e)
        {
            if (sfxOn == true)
            {
                click01.Play();
            }
            // When the "Play" button is tapped, we load the GameplayScreen
            LoadingScreen.Load(ScreenManager, true, PlayerIndex.One, new GameplayScreen());
        }

        void sfxButton_Tapped(object sender, EventArgs e)
        {
            BooleanButton button = sender as BooleanButton;

            // In a real game, you'd want to store away the value of 
            // the button to turn off sounds here. :)
            if (sfxOn == true)
            {   
                sfxOn = false;
                
            }
            else if (sfxOn == false)
            {
                sfxOn = true;
                
            }

        }

        void musicButton_Tapped(object sender, EventArgs e)
        {
            BooleanButton button = sender as BooleanButton;
            
            if (bgmOn==true)
            {
                bgmOn = false;
            }else if(bgmOn==false)
            {
                bgmOn = true;
            }
            toogleMusic(bgmOn);
        }

        void helpButton_Tapped(object sender, EventArgs e)
        {
            if (sfxOn == true)
            {
                click01.Play();
            }
            LoadingScreen.Load(ScreenManager, true, PlayerIndex.One, new GameStateManagementSample.Screens.Help());

        }

        void creditsButton_Tapped(object sender, EventArgs e)
        {
            if (sfxOn == true)
            {
                click01.Play();
            }
            LoadingScreen.Load(ScreenManager, true, PlayerIndex.One, new GameStateManagementSample.Screens.creditsPage());

        }

        protected override void OnCancel()
        {
            ScreenManager.Game.Exit();
            base.OnCancel();
        }

        void toogleMusic(bool b)
        {

            if (b == true)
            {
                musicManager.Play(bg);
            }
            else
            {
                musicManager.Stop();
            }

        }
    }
}
