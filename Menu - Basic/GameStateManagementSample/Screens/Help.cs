using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Phone.Controls;
using GameStateManagement;
namespace GameStateManagementSample.Screens
{
    class Help : GameScreen
    {
        Texture2D backgroundPic;
        InputAction backAction;
        SpriteFont text;
        Texture2D spLogo;

        public Help()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            backAction = new InputAction(
                new Buttons[] { Buttons.Start, Buttons.Back },
                new Keys[] { Keys.Escape },
                true);

        }
        public override void Activate(bool instancePreserved)
        {
            backgroundPic = ScreenManager.Game.Content.Load<Texture2D>("menu");
            text = ScreenManager.Game.Content.Load<SpriteFont>("creditFont");

            spLogo = ScreenManager.Game.Content.Load<Texture2D>("splogo");
            base.Activate(instancePreserved);
        }
        public override void Deactivate()
        {
            base.Deactivate();
        }
        public override void Unload()
        {
            base.Unload();
        }
        public override void HandleInput(GameTime gameTime, InputState input)
        {   // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            PlayerIndex playerindex;
            if (backAction.Evaluate(input, ControllingPlayer, out playerindex) || gamePadDisconnected)
            {
                LoadingScreen.Load(ScreenManager, false, null, new BackgroundScreen(),
                                                           new PhoneMainMenuScreen());
            }

            base.HandleInput(gameTime, input);
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            spriteBatch.Begin();
            spriteBatch.Draw(backgroundPic, new Rectangle(0, 0, viewport.Width, viewport.Height), new Color(TransitionAlpha, TransitionAlpha, TransitionAlpha));
            spriteBatch.DrawString(text, "The long awaited Phone Twister!", new Vector2(10, 160), Color.Black);
            spriteBatch.DrawString(text, "Twist,rotate, shift your arms", new Vector2(10, 220), Color.Black);
            spriteBatch.DrawString(text, "all for the survival of your finger.", new Vector2(10, 260), Color.Black);
            spriteBatch.DrawString(text, "Sabotage your friend by tickling", new Vector2(10, 300), Color.Black);
            spriteBatch.DrawString(text, "or work together and survive.", new Vector2(10, 340), Color.Black);
            spriteBatch.DrawString(text, "PS: No fingers were harmed in", new Vector2(10, 380), Color.Black);
            spriteBatch.DrawString(text, "the development of the game.", new Vector2(10, 420), Color.Black);
           
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
