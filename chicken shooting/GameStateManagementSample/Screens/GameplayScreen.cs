#region File Description
//-----------------------------------------------------------------------------
// GameplayScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Threading;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
#endregion

namespace GameStateManagement
{
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>
    class GameplayScreen : GameScreen
    {
        #region Fields

        ContentManager content;
        SpriteFont gameFont;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;
        Texture2D white;
        Color myColor;
        int rDirection = 2;
        string info = "";
        Texture2D ball;
        Texture2D bullet;
        int ballFSM = 0;
        int bulletFSM = 0;
        Rectangle ballSpawn = new Rectangle(0, 0, 64, 64);
        Rectangle bulletPosition = new Rectangle(0, 0, 16, 16);
        double angle = 0;
        int yDelta = 0;
        bool spawnAbove = false;

        Rectangle backgroundRectangle = new Rectangle(0, 0, 480, 800);

        Random random = new Random();

        #endregion

        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        public GameplayScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            TouchPanel.EnabledGestures = GestureType.Tap | GestureType.FreeDrag | GestureType.DragComplete;
        }//gameplay screen constructor


        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            gameFont = content.Load<SpriteFont>("gamefont");

            // A real game would probably have more content than this sample, so
            // it would take longer to load. We simulate that by delaying for a
            // while, giving you a chance to admire the beautiful loading screen.
            Thread.Sleep(1000);

            white = this.ScreenManager.Game.Content.Load<Texture2D>("white");
            font = this.ScreenManager.Game.Content.Load<SpriteFont>("font");
            myColor = new Color(150, 100, 100);
            ball = this.ScreenManager.Game.Content.Load<Texture2D>("ball");
            bullet = this.ScreenManager.Game.Content.Load<Texture2D>("bullet");

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }//load content


        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }//unload content


        #endregion

        #region Update and Draw


        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            if (IsActive)
            {
                // TODO: Add your update logic here
                myColor.R += (Byte)rDirection;

                if (myColor.R >= 250)
                    rDirection = -8;
                if (myColor.R <= 150)
                    rDirection = 8;

                if (spawnAbove)
                {
                    bulletPosition.X += (int)(yDelta * Math.Tan(angle) / 20);
                    bulletPosition.Y += (int)(-1 * yDelta / 20);
                }//if spawn above spawn
                else
                {
                    bulletPosition.X += (int)(yDelta * Math.Tan(angle) / 20);
                    bulletPosition.Y += (int)(yDelta / 20);
                }//if spawnAbove below spawn
            

                // TODO: this game isn't very fun! You could probably improve
                // it by inserting something more interesting in this space :-)
            }//if is active
        }//update


        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            PlayerIndex player;
            if (pauseAction.Evaluate(input, ControllingPlayer, out player) || gamePadDisconnected)
            {
#if WINDOWS_PHONE
                ScreenManager.AddScreen(new PhonePauseScreen(), ControllingPlayer);
#else
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
#endif
            }
            else
            {

            }
            foreach (GestureSample sample in input.Gestures)
            {
                if (sample.GestureType == GestureType.Tap)
                {
                    ballFSM = 1;
                    ballSpawn.X = (int)sample.Position.X - 32;
                    ballSpawn.Y = (int)sample.Position.Y - 32;
                }

                if (sample.GestureType == GestureType.FreeDrag)
                {
                    bulletFSM = 1;
                    bulletPosition.X = (int)sample.Position.X;
                    bulletPosition.Y = (int)sample.Position.Y;
                    if (sample.Position.Y >= ballSpawn.Y)
                    {
                        spawnAbove = false;
                        angle = Math.Atan((sample.Position.X - ballSpawn.X - 32) / (sample.Position.Y - ballSpawn.Y - 32));
                    }
                    else
                    {
                        spawnAbove = true;
                        angle = (-1) * (Math.Atan((sample.Position.X - ballSpawn.X - 32) / (sample.Position.Y - ballSpawn.Y - 32)));
                    }

                }

                if (sample.GestureType == GestureType.DragComplete)
                {
                    bulletFSM = 1;
                    yDelta = (int)(sample.Position.X - ballSpawn.X - 32);
                    //angle = Math.Atan((sample.Position.X - ballSpawn.X) / (sample.Position.Y - ballSpawn.Y));
                }
            }//input

        }//handle input


        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // This game has a blue background. Why? Because!
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.CornflowerBlue, 0, 0);

            // Our player and enemy are both actually just text strings.
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin();
            spriteBatch.Draw(white, backgroundRectangle, myColor);
            spriteBatch.DrawString(font, myColor.R.ToString(), new Vector2(10, 10), Color.White);
            spriteBatch.DrawString(font, myColor.G.ToString(), new Vector2(10, 30), Color.White);
            spriteBatch.DrawString(font, myColor.B.ToString(), new Vector2(10, 50), Color.White);
            spriteBatch.DrawString(font, myColor.A.ToString(), new Vector2(10, 70), Color.White);
            spriteBatch.DrawString(font, info, new Vector2(50, 100), Color.Azure);

            if (ballFSM == 1)
            {
                spriteBatch.Draw(ball, ballSpawn, Color.White);
            }//if ballFSM == 1, means a spawn should be drawn
            if (bulletFSM == 1)
            {
                spriteBatch.Draw(bullet, bulletPosition, Color.White);
            }//if bulletFSM == 1, means a bullet should be drawn

            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0)
                ScreenManager.FadeBackBufferToBlack(1f - TransitionAlpha);
        }//draw


        #endregion
    }//class gameplay screen
}//namespace